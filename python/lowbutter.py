import scipy.signal as sig
import scipy.constants as const
import numpy as np
twopies= 2.0*const.pi
freq= 1500000
omega= twopies*freq
omega= 1.0
for n in range(3,8,2):
    (z,p,k)=sig.butter(n, omega, btype='lowpass', analog=True, output='zpk')
    print 'poles: %(poles)s; gain: %(gain)s' % dict(poles=p, gain=k)
    
